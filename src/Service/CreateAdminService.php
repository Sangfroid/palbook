<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateAdminService
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UserPasswordHasherInterface $userPasswordHasherInterface
    ) {
    }

    public function create(string $email, string $password): void
    {
        $user = $this->userRepository->findOneBy(['email' => $email]);

        if (!$user) {
            // throw new \InvalidArgumentException('User already exists.');
            $user = new User();

            $user->setEmail($email);
            $password = $this->userPasswordHasherInterface->hashPassword($user, $password);
            $user->setPassword($password);
        }
        $user->setRoles(['ROLE_ADMIN']);

        $this->userRepository->save($user, true);
    }
}
